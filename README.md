# SMMA IT Flutter Test



## Welcome!

Selamat Datang di SMMA IT Flutter Test!

Repository ini akan menjadi sarana untuk lebih mengetahui kemampuan Anda dalam membuat aplikasi mobile berbasis flutter.

Anda akan diminta untuk membuat sebuah aplikasi mobile dengan flutter dengan sebuah narasi yang akan disebutkan dalam soal.

Untuk memulai, silahkan lanjut ke [soal](#soal).

## Soal

>Buatlah sebuah aplikasi mobile untuk penggemar game Mobile Lagend Bang Bang(MLBB). Aplikasi ini akan menampilkan list dari hero MLBB yang terdapat daalam game. Aplikasi tersebut haruslah memiliki fitur sebagai berikut: 
  >- Terdapat fitur login dan logout
  >- Memiliki fitur register yang mengharuskan pengguna untuk memasukkan data sebagai berikut:
  >   - Nama lengkap
  >   - Alamat email
  >   - Gender (laki-laki atau perempuan)
  >   - Tanggal lahir
  >   - Provinsi tempat lahir. Untuk pilihan kota bisa diambil dari ```http://www.emsifa.com/api-wilayah-indonesia/api/provinces.json```
  >   - Foto pengguna menggukan kamera
  >- Terdapat halaman profil yang menampilkan hasil input di halaman register
  >- Manampilkan list hero MLBB. Data hero MLBB dapat diambil dari ```https://api.dazelpro.com/mobile-legends/hero```. Mohon dicatat ini adalah API tidak resmi. Jadi tidak bisa menampilkan gambar (eror 404)
  >- Terdapat fitur untuk memilih dan menampilkan hero favorit


## Ketentuan
  Berikut ketentuan dalam mengerjakan soal di atas
  - Pastikan aplikasi Flutter Anda dapat di-run dari komputer manapun
  - Pastikan ketentuan yang terdapat pada [Soal](#soal) di atas terpenuhi
  - Diwajibkan untuk menggunakan state management. **Tetapi dilarang menggunakan GetX**
  - REST API yang disebutkan dalam soal merupakan opsi. Diperbolehkan mengganti dengan REST API lain jika dirasa perlu
  - Tampilan tidak akan menjadi penilaian utama, tetapi tampilan yang baik akan menjadi nilai plus
  - Penyimpanan data menggunakan server Back-end yang ter-deploy dan database akan menjadi nilai lebih
  - Penambahan fitur apapun yang Anda lakukan pada aplikasi Anda akan menjadi nilai tambah

## Petunjuk pengerjaan

  - Silahkan fork repositori ini, kemudian lanjutkan mengerjakan di repo hasil fork tersebut
  - Dimohon untuk commit dan push pekerjaan Anda sesering mungkin
  - Batas akhir pengerjaan:
    > Kurang lebih 50 jam setelah Anda menerima email soal yang berisi tautan ke repo ini. Waktu jelasnya dapat dilihat di email yang dikirimkan ke email Anda
  - Jika sudah selesai mohon lakukan pull request ke repo utama dengan ketentuan nama pull request:
    > Nama lengkap anda dipisahkan dengan strip (-) dengan huruf kecil semua (contoh: romli-anwar)

## Catatan
  Dokumentasi lengkap untuk REST API di atas dapat diakses lewat tautan berikut:
  - [REST API MLBB (unofficial)](https://dazelpro.com/api/mobile-legends)
  - [REST API Data Statis Provinsi](https://www.emsifa.com/api-wilayah-indonesia/)

  Jika ada pertanyaan silahkan menghubungi lewat email ridza.adhandra@smma.co.id


## **Selamat mengerjakan!**


